# FROM openjdk:8-jdk-alpine AS builder
# LABEL stage=builder
# WORKDIR /app
# ADD ./ /app/
# RUN cd /app
# RUN ./mvnw clean install spring-boot:repackage
# 
# FROM openjdk:8-jdk-alpine
# WORKDIR /app
# COPY --from=builder /app/target/cdaas-demo-app-*.jar app.jar
# EXPOSE 8080
# ENTRYPOINT [ "java", "-jar", "/app/app.jar" ]


FROM python:3.8.1-alpine
# Install software to Alpine Linux image
RUN apk update && \
    apk add --no-cache bash \
    musl-dev \
    libressl-dev \
    gcc \
    libffi-dev \
    make \
    openssh \
    sshpass \
    curl && \
  rm -rf /var/lib/apt/lists/*

# Configure SSH for connecting to hadoop edge
RUN mkdir -p /root/.ssh/ && \
    mkdir -p /etc/ssh/
# Create workdir   into the container
RUN mkdir -p /ansible
COPY ./ansible_cd/* /ansible/
RUN chmod -R 600 /root/.ssh

# # Install KPN certificate
# RUN curl -X GET https://artifacts.kpn.org/d-nitro-local/scripts/linux_trust_ca_cert_1.0.4.sh | bash -s
# # Configure pip for access to Artifactory
# RUN pip config set global.'index-url' 'https://artifacts.kpn.org/api/pypi/pypi/simple' && \
#     pip config set global.'trusted-host' 'artifacts.kpn.org'
RUN apk add --update py-pip
# Set workdir for docker
WORKDIR /ansible
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
# Build and install dependencies
ADD ./requirements.txt /ansible/requirements.txt
# RUN apk add --update alpine-sdk && apk add libffi-dev openssl-dev
RUN apk add --update alpine-sdk
RUN apk add libffi-dev openssl-dev
RUN apk --no-cache --update add build-base
RUN apk update && apk add --no-cache --virtual docker-cli python3-dev libffi-dev openssl-dev gcc libc-dev make python3 py3-pip py-pip curl libffi-dev openssl-dev gcc libc-dev rust cargo make
RUN pip install --upgrade pip && \
    # pip install --upgrade ansible && \
    pip install -r requirements.txt
