# Spring Boot Demo App

This repository is used to test the application that is being deployed by https://github.com/ltutar/playbook-awx-artifactory-download-and-deploy

The file .gitlab-ci.yml can be adapted to fit with the desired build and deploy workflow for the team. The main parts here are:

stages: This is simple enough, that stages are part of the pipeline.

# image: 
This tells the Gitlab Runner which docker image to use for running the scripts for the jobs. Here it is specified globally, but it can be set for each job individually. The image being pulled here is a python alpine image with Ansible, openssh and pyYAML installed, and pushed to Gitlab Registry. 

# variables: 
Environment variables within the scope of the docker image. Here, we add some variables for Ansible such as auto-accepting host ssh certs and visibility.

# .run_playbook: 
Again, specified globally as we´d like to use the same script for each job. This script is run in the shell of the docker image. The first block of lines in the script here pulls the private part of a ssh keypair from a Gitlab CI/CD variable and does some trimming to satisfy the ssh-clients, who is a picky eater. The variable SSH_PRIVATE_KEY is set in the project, under Settings -> CI/CD -> Variables. The last command line runs the playbook, specifying the inventory and additional settings.

This ansible playbook is run to download, deploy and test spring boot application via a CD pipeline.

# deploy_dev/deploy_test: 
This is the job declarations, specifying additional variables such as the Ansible host group to target, the inventory. In this case the stage-job relationship is 1–1, but one can add more jobs within a stage, such as end-point testing, registering with monitoring and so on.


# Build
## Maven
Validate

```bash
./mvnw clean validate
```

Compile

```bash
./mvnw clean compile
```

Test and create a report

```bash
./mvnw surefire-report:report
```

Build

```bash
./mvnw clean install spring-boot:repackage
```

Build a specific version

```bash
./mvnw versions:set -DnewVersion=1.0.0
./mvnw clean install spring-boot:repackage
```

## Running the application as jar

```bash
java -jar ./target/cdaas-demo-app-1.0.0.jar
```

## Docker
Build image

```bash
docker build -t cdaas-demo-app:1.0.0 .
```

## Running the application as docker container

```bash
docker run -d cdaas-demo-app:1.0.0
```

## Running the application as docker container using Ansible

```bash
sudo -E ansible-playbook download_and_run_tests.yml --private-key=~/.ssh/id_rsa \
      -i $inventory \
      -e "app_servers=$hosts"\
      -e IMAGE_TAG="$IMAGE_TAG"
      
```

### Note: The CI part has been tested. The CD part is not fully tested but completed the coding part.
# I can explain in the interview how I can make it run.

# Application endpoints

Replace localhost accordingly.

- Main application: http://localhost:8080/
- Actuator endpoint: http://localhost:8080/actuator
- Health: http://localhost:8080/actuator/health

![<img src="images/java-end-result.png" height="400">](images/java-end-result.png)
